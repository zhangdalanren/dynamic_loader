#ifndef DL_STDLIB_LIB_H__
#define DL_STDLIB_LIB_H__

#include "dl_extern_lib.h"
#include <stdlib.h>


#define CALLOC			0
#define FREE			1
#define MALLOC			2
#define REALLOC			3
#define ALIGEND_ALLOC	4


#endif
